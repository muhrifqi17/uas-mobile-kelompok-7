import 'package:flutter/material.dart';
import 'package:kelompok_7/pages/dosen_ta_page.dart';
import 'package:kelompok_7/pages/tugas_akhir.dart';
import 'package:kelompok_7/pages/create_tugas_akhir_page.dart';

void main() {
  runApp(
    MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: '/tugas-akhir',
      routes: {
        '/tugas-akhir': (context) => TugasAkhirPage(),
        '/create-tugas-akhir-page': (context) => CreateTugasAkhirPage(),
        '/dosen-ta-page': (contex) => DosenTAPage(),
      },
    ),
  );
}
