import 'package:flutter/material.dart';
import 'package:kelompok_7/pages/dosen_ta_page.dart';

class TugasAkhirPage extends StatefulWidget {

  @override
  _TugasAkhirState createState() => _TugasAkhirState();
}

class _TugasAkhirState extends State<TugasAkhirPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Tugas Akhir"),
        backgroundColor: Color(0xFF005BAA),
      ),
      body: Container(
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            Container(
              child: ListTile(
                title: Text(
                  "Dosen",
                  style: TextStyle(fontSize: 25),
                ),
                trailing: Stack(
                  children: [
                    Text(
                      "Jml Mhs",
                      style: TextStyle(fontSize: 25),
                    )
                  ],
                ),
              ),
            ),
            Card(
              child: ListTile(
                title: Text(
                  "Hilmy A. T.",
                  style: TextStyle(fontSize: 20),
                ),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => DosenTAPage(value : "Hilmy A. T.")
                    ),
                  );
                },
                trailing: Stack(
                  children: [
                    Text(
                      "6",
                      style: TextStyle(fontSize: 20),
                    )
                  ],
                ),
              ),
            ),
            Card(
              child: ListTile(
                title: Text(
                  "Sirojul Munir",
                  style: TextStyle(fontSize: 20),
                ),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => DosenTAPage(value : "Sirojul Munir")),
                  );
                },
                trailing: Stack(
                  children: [
                    Text(
                      "7",
                      style: TextStyle(fontSize: 20),
                    )
                  ],
                ),
              ),
            ),
            Card(
              child: ListTile(
                title: Text(
                  "Zaki I",
                  style: TextStyle(fontSize: 20),
                ),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => DosenTAPage(value : "Zaki I")),
                  );
                },
                trailing: Stack(
                  children: [
                    Text(
                      "5",
                      style: TextStyle(fontSize: 20),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
