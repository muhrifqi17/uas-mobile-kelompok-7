import 'package:flutter/material.dart';
import 'package:kelompok_7/pages/create_tugas_akhir_page.dart';

class DosenTAPage extends StatefulWidget {

  String value;
  DosenTAPage({this.value});

  @override
  _DosenTAPageState createState() => _DosenTAPageState(value);
}

class _DosenTAPageState extends State<DosenTAPage> {

  String value;
  _DosenTAPageState(this.value);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Dosen TA"),
        backgroundColor: Color(0xFF005BAA),
      ),

      body: Container(
        child: ListView(
          children: [
            if (value == "Hilmy A. T.")
              Container(
                child: ListView(
                  shrinkWrap: true,
                  children: [
                    Card(
              child: ListTile(
                title: Text(
                  "Hilmy A. T.",
                  style: TextStyle(fontSize: 20),
                ),
              ),
            ),
            Container(
              child: ListTile(
                title: Text(
                  "Bidang Penelitian : ",
                  style: TextStyle(
                    fontSize: 20,
                  ),
                ),
              ),
            ),
            Container(
                child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        vertical: 00.0,
                        horizontal: 20,
                      ),
                      child: Text(
                        "-Kecerdasan Buatan",
                        style: TextStyle(
                          fontSize: 20.0,
                        ),
                      ),
                    )
                  ],
                ),
                Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        vertical: 00.0,
                        horizontal: 20,
                      ),
                      child: Text(
                        "-Data Mining",
                        style: TextStyle(
                          fontSize: 20.0,
                        ),
                      ),
                    )
                  ],
                ),
                Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        vertical: 00.0,
                        horizontal: 20,
                      ),
                      child: Text(
                        "-Mobile App Dev",
                        style: TextStyle(
                          fontSize: 20.0,
                        ),
                      ),
                    )
                  ],
                ),
                Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        vertical: 00.0,
                        horizontal: 20,
                      ),
                      child: Text(
                        "-Computer Vision",
                        style: TextStyle(
                          fontSize: 20.0,
                        ),
                      ),
                    )
                  ],
                ),
                Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        vertical: 20,
                        horizontal: 20,
                      ),
                      child: Text(
                        "Topik TA",
                        style: TextStyle(
                          fontSize: 20.0,
                        ),
                      ),
                    )
                  ],
                ),
              ],
            )),
            SizedBox(
              width: double.infinity,
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 300.0,
                  horizontal: 20,
                ),
                child: RaisedButton(
                  padding: const EdgeInsets.symmetric(
                    vertical: 20,
                    horizontal: 10,
                  ),
                  color: Colors.orange,
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => CreateTugasAkhirPage(values : value)),
                    );
                  },
                  child: Text(
                    'Pilih Dosen',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                    ),
                  ),
                ),
              ),
            ),

                  ],
                )
                )
            else if (value == "Sirojul Munir")
              Container(
                child: ListView(
                  shrinkWrap: true,
                  children: [
                    Card(
              child: ListTile(
                title: Text(
                  "Sirojul Munir",
                  style: TextStyle(fontSize: 20),
                ),
              ),
            ),
            Container(
              child: ListTile(
                title: Text(
                  "Bidang Penelitian : ",
                  style: TextStyle(
                    fontSize: 20,
                  ),
                ),
              ),
            ),
            Container(
                child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        vertical: 00.0,
                        horizontal: 20,
                      ),
                      child: Text(
                        "-Kecerdasan Buatan",
                        style: TextStyle(
                          fontSize: 20.0,
                        ),
                      ),
                    )
                  ],
                ),
                Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        vertical: 00.0,
                        horizontal: 20,
                      ),
                      child: Text(
                        "-Data Mining",
                        style: TextStyle(
                          fontSize: 20.0,
                        ),
                      ),
                    )
                  ],
                ),
                Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        vertical: 00.0,
                        horizontal: 20,
                      ),
                      child: Text(
                        "-Mobile App Dev",
                        style: TextStyle(
                          fontSize: 20.0,
                        ),
                      ),
                    )
                  ],
                ),
                Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        vertical: 00.0,
                        horizontal: 20,
                      ),
                      child: Text(
                        "-Computer Vision",
                        style: TextStyle(
                          fontSize: 20.0,
                        ),
                      ),
                    )
                  ],
                ),
                Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        vertical: 20,
                        horizontal: 20,
                      ),
                      child: Text(
                        "Topik TA",
                        style: TextStyle(
                          fontSize: 20.0,
                        ),
                      ),
                    )
                  ],
                ),
              ],
            )),
            SizedBox(
              width: double.infinity,
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 300.0,
                  horizontal: 20,
                ),
                child: RaisedButton(
                  padding: const EdgeInsets.symmetric(
                    vertical: 20,
                    horizontal: 10,
                  ),
                  color: Colors.orange,
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => CreateTugasAkhirPage(values : value)),
                    );
                  },
                  child: Text(
                    'Pilih Dosen',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                    ),
                  ),
                ),
              ),
            ),

                  ],
                )
                )
            else if (value == "Zaki I")
              Container(
                child: ListView(
                  shrinkWrap: true,
                  children: [
                    Card(
              child: ListTile(
                title: Text(
                  "Zaki I",
                  style: TextStyle(fontSize: 20),
                ),
              ),
            ),
            Container(
              child: ListTile(
                title: Text(
                  "Bidang Penelitian : ",
                  style: TextStyle(
                    fontSize: 20,
                  ),
                ),
              ),
            ),
            Container(
                child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        vertical: 00.0,
                        horizontal: 20,
                      ),
                      child: Text(
                        "-Kecerdasan Buatan",
                        style: TextStyle(
                          fontSize: 20.0,
                        ),
                      ),
                    )
                  ],
                ),
                Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        vertical: 00.0,
                        horizontal: 20,
                      ),
                      child: Text(
                        "-Data Mining",
                        style: TextStyle(
                          fontSize: 20.0,
                        ),
                      ),
                    )
                  ],
                ),
                Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        vertical: 00.0,
                        horizontal: 20,
                      ),
                      child: Text(
                        "-Mobile App Dev",
                        style: TextStyle(
                          fontSize: 20.0,
                        ),
                      ),
                    )
                  ],
                ),
                Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        vertical: 00.0,
                        horizontal: 20,
                      ),
                      child: Text(
                        "-Computer Vision",
                        style: TextStyle(
                          fontSize: 20.0,
                        ),
                      ),
                    )
                  ],
                ),
                Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        vertical: 20,
                        horizontal: 20,
                      ),
                      child: Text(
                        "Topik TA",
                        style: TextStyle(
                          fontSize: 20.0,
                        ),
                      ),
                    )
                  ],
                ),
              ],
            )),
            SizedBox(
              width: double.infinity,
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 300.0,
                  horizontal: 20,
                ),
                child: RaisedButton(
                  padding: const EdgeInsets.symmetric(
                    vertical: 20,
                    horizontal: 10,
                  ),
                  color: Colors.orange,
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => CreateTugasAkhirPage()),
                    );
                  },
                  child: Text(
                    'Pilih Dosen',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                    ),
                  ),
                ),
              ),
            ),

                  ],
                )
                )
          ],
        ),
      )

    );
  }
}
